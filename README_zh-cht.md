馬克記帳
======
「馬克記帳」是一款以 Markdown 格式為基礎的記帳軟體。

語言
------
+ [English](README.md)
+ [简体中文](README_zh-chs.md)


功能特色
------
### Markdown 格式
![feature-markdown](assets/zh-cht/feature-markdown.png)

構成帳本格式的三層結構，以 Markdown 語法中的「標題」、「子標題」與「項目」符號，分別對應「日期」、「成員」與「品項」元素。

    (在檔案瀏覽器開啟 `.md` 檔案後，右上角的 MD 按鈕可以檢視純文字內容。) 

### 快選清單
![feature-select](assets/zh-cht/feature-select.png)

這些帳本元素，擁有獨立的模板配置檔案。

在記錄時，可以動態擴增並且使用自定義的選單，迅速的完成編輯動作。

### 四則運算
![feature-calculate](assets/zh-cht/feature-calculate.png)

輸入品項的金額時，支援四則運算。

除了詳細記錄數字的產生過程，每個成員的品項會自動加總並且統計在列表底部，可供相關人員後續結算。

### 共同編輯
![feature-share-file](assets/zh-cht/feature-share-file.png)

透過 iCloud 服務可以同步帳本記錄到電腦，或者使用檔案分享功能，邀請成員共同編輯。

由於純文字檔案特性，也可以很容易的二次應用或者版本控制。

### 客製化
![feature-customized](assets/zh-cht/feature-customized.png)

日期部分可以調整成一般文字，加上計量單位的修改，就能變成另外一種格式的帳本。

    (在 Sample/ 資料夾中，示範了五種模板範例: 單人、雙人、多人、活動、時程。)


聯絡我們
------
如果發現任何問題或者有任何更好的建議，歡迎聯繫我們 ~ !!!

### rhxs020@gmail.com


下載
------
+ [範例](Sample/zh-cht/)
+ [模板配置檔](SysConfig/zh-cht/template-config.md)


Gamma Ray 軟體工作室
------
### Youtube
<https://www.youtube.com/user/rhxs020>

### Instagram
<https://www.instagram.com/gammaray_studio/>

### Facebook
<https://www.facebook.com/gammaray.studio>

