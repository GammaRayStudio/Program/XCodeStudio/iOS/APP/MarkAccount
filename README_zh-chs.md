马克记帐
======
「马克记帐」是一款以 Markdown 格式为基础的记帐软体。

语言
------
+ [English](README.md)
+ [繁體中文](README_zh-cht.md)


功能特色
------
### Markdown 格式
![feature-markdown](assets/zh-chs/feature-markdown.png)

构成帐本格式的三层结构，以 Markdown 语法中的「标题」、「子标题」与「项目」符号，分别对应「日期」、「成员」与「品项」元素。

    (在档案浏览器开启 `.md` 档案后，右上角的 MD 按钮可以检视纯文字内容。) 

### 快选清单
![feature-select](assets/zh-chs/feature-select.png)

这些帐本元素，拥有独立的模板配置档案。

在记录时，可以动态扩增并且使用自定义的选单，迅速的完成编辑动作。

### 四则运算
![feature-calculate](assets/zh-chs/feature-calculate.png)

输入品项的金额时，支援四则运算。

除了详细记录数字的产生过程，每个成员的品项会自动加总并且统计在列表底部，可供相关人员后续结算。

### 共同编辑
![feature-share-file](assets/zh-chs/feature-share-file.png)

透过 iCloud 服务可以同步帐本记录到电脑，或者使用档案分享功能，邀请成员共同编辑。

由于纯文字档案特性，也可以很容易的二次应用或者版本控制。

### 客制化
![feature-customized](assets/zh-chs/feature-customized.png)

日期部分可以调整成一般文字，加上计量单位的修改，就能变成另外一种格式的帐本。

    (在 Sample/ 资料夹中，示范了五种模板范例: 单人、双人、多人、活动、时程。)


联络我们
------
如果发现任何问题或者有任何更好的建议，欢迎联系我们 ~ !!!

### rhxs020@gmail.com


下载
------
+ [范例](Sample/zh-chs/)
+ [模板配置档](SysConfig/zh-chs/template-config.md)


Gamma Ray 软体工作室
------
### Youtube
<https://www.youtube.com/user/rhxs020>

### Instagram
<https://www.instagram.com/gammaray_studio/>

### Facebook
<https://www.facebook.com/gammaray.studio>

