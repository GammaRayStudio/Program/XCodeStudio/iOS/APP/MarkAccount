Mark Account
======
"Mark Account" is a Markdown-based accounting App.

Language
------
+ [繁體中文](README_zh-cht.md)
+ [简体中文](README_zh-chs.md)


Features
------
### Markdown
![feature-markdown](assets/en-us/feature-markdown.png)

Constructs a three-level structure in account book format with Markdown syntax title, subtitle, and item symbols, representing date, member, and item elements respectively.

    After opening the `.md` file in the file browser,
    click the MD button in the upper right corner to view the plain text content.

### Quick Pick List
![feature-select](assets/en-us/feature-select.png)

There are independent template configuration files in these account book elements.

When recording, you can dynamically add and use custom list form to quickly complete edit.

### Calculate
![feature-calculate](assets/en-us/feature-calculate.png)

When entering the amount of an item, arithmetic operations are supported.

In addition to the detailed record of the number generation process, the items of each member will be automatically added and counted at the bottom of the list for subsequent calculation by relevant personnel.

### Co-editing
![feature-share-file](assets/en-us/feature-share-file.png)

You can sync your records of account book to computer through iCloud service, or use the file sharing function to invite members to edit together.

Due to the attribute of plain text file, it's also easy to use secondary applications or version control.

### Customized
![feature-customized](assets/en-us/feature-customized.png)

The date section can be adjusted to the general text. With the modification of the measurement units, it can be turned into another format of the account book.


    In the Sample/ folder, five template examples are demonstrated: 
    Single, Double, Multiple, Activity, and Schedule.
    

Contact us
------
If you find any problems or have any better suggestions, please feel free to contact us ~ !!!

### rhxs020@gmail.com


Download
------
+ [Sample](Sample/en-us/)
+ [Template configuration file](SysConfig/en-us/template-config.md)


Gamma Ray Studio
------
### Youtube
<https://www.youtube.com/user/rhxs020>

### Instagram
<https://www.instagram.com/gammaray_studio/>

### Facebook
<https://www.facebook.com/gammaray.studio>

